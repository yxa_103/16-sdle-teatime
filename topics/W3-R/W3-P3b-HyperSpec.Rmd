---
title: "Tea Time-HyperSpec package"
author: "Donghui"
date: "June 23, 2016"
output: html_document
---



```
```{r}

#HyperSpec is a R package that allows conveniant handling of hyperspectral data sets. 

##The spectra can be anything that is recorded over a common disretized axis

## five data sets (chondro, flu, laser, paracetamol and barbiturates) and each of data comes with its own vignette. 
#1. structure of hyperSpec object
library(hyperSpec)
## Not run: vignette ("baseline", package = "hyperSpec")
str(chondro)
##the structure hyperspec object
str(chondro[c(1,101)])
##a hyperSpec object contains 4 slots
#@wavelength    numeric vector with wavelength acis of the spectrum
#@data      a data.frame with spectra and all information belonging to the spectra
#@label     labels(particularly for axis annotations)
#@log       additional information (usually is empty)

#2.create new hyperSpec objects from the spectra matrix and wavelength vector

data1<-chondro[c(1,43,100)]

new.hyperSpec <- new("hyperSpec", spc =data1, wavelength = corrected@wavelength, label = list(spc = "A / a.u.", .wavelength = paste("Wavenumber","/",expression (cm^-1),sep = " ")))

##reverse wavenumber and add legend
plot(new.hyperSpec,wl.reverse = TRUE,col=c(1:3))
legend(1150, 1500, legend=c("data1", "data2", "data3"), col=c(1:3),lty=1)
text(1150, 1550, "chondro", cex = 1.8)
str(new.hyperSpec)
class(new.hyperSpec)
#object transformation
as.matrix(new.hyperSpec)





#3.baseline correction 

 bl <- spc.fit.poly (chondro [c (1, 101),, c (633~640, 1788~1790)], chondro [c (1, 101)], poly.order = 1)
##spc.fit.poly calsulates the least squares fit of order poly.order

 plot (chondro [c (1, 101)], plot.args = list (ylim = c(200, 1200)), col = 1 : 2)
 plot (chondro [c (1, 101),, c (633~640, 1788~1790)], add = TRUE, col = 1:2, lines.args = list (type = "p", pch = 20))
 plot (bl, add = TRUE, col = 1 : 2)

##Stacked spectra
plotspc(corrected,col=1:2,stacked = TRUE)
##waveranges
plotspc(corrected,col=1:2,stacked = TRUE, wl.range = c (600 ~ 800, 1200 ~ max))
##two types of function for baseline function
baselines <- spc.fit.poly(chondro[,, c (625 ~ 640, 1785 ~ 1800)], chondro)
plot(chondro - baselines, "spcprctl5",plot.args = list (ylim = c(-10, 200)))
baselines <- spc.fit.poly.below(chondro, npts.min = 30)
plot(chondro - baselines, "spcprctl5",plot.args = list (ylim = c(-10, 200)))


#4. hyperSpec object for other packages
library(rgl)
a <- corrected
a$t <- 1:2
cols <- rep (matlab.palette (nrow (a)), nwl (a))
 surface3d (y = wl (a), x = a$t, z = a$spc, col = cols)
 aspect3d (c (0.5, 1, 0.25))
 axes3d (c ('x+-', 'y--', 'z--'))
 axes3d ('x+-', ntrick=0, labels = FALSE)
 axes3d ('y--', nticks = 25, labels= FALSE)
 mtext3d ("repeating sample No.", 'x+-', line=2.5)
 mtext3d ("lambda^-1 / nm^-1", 'y--', line = 2.5)
 mtext3d ("Abs / a.u.", edge = 'z--', line = 2.5)
```

